import pytest

from fusionprov import mastprov


file_params = {"data": "alp", "shot": 30420, "run": None, "graph": False}
signal_params = {"data": "ip", "shot": 30420, "run": None, "graph": False}
image_params = {"data": "rir", "shot": 30420, "run": None, "graph": False}
invalid_params = {"data": "foobar", "shot": 30420, "run": None, "graph": False}


@pytest.mark.parametrize(
    "params, expected_result",
    [
        (file_params, "data_file"),
        (signal_params, "uda_signal"),
        (image_params, "image_file"),
    ],
)
def test_guess_data_type(params, expected_result):
    assert mastprov.guess_data_type(params) == expected_result


def test_guess_data_type_invalid():
    with pytest.raises(SystemExit) as e:
        mastprov.guess_data_type(invalid_params)

    assert e.type == SystemExit
    assert e.value.code == 1


@pytest.mark.parametrize(
    "params, expected_result",
    [
        (file_params, True),
        (signal_params, False),
        (image_params, False),
        (invalid_params, False),
    ],
)
def test_MastFileProv_validator(params, expected_result):
    assert mastprov.MastFileProv.validate(params) == expected_result


@pytest.mark.parametrize(
    "params, expected_result",
    [
        (file_params, False),
        (signal_params, True),
        (image_params, False),
        (invalid_params, False),
    ],
)
def test_MastSignalProv_validator(params, expected_result):
    assert mastprov.MastSignalProv.validate(params) == expected_result


@pytest.mark.parametrize(
    "params, expected_result",
    [
        (file_params, False),
        (signal_params, False),
        (image_params, True),
        (invalid_params, False),
    ],
)
def test_MastSignalProv_validator(params, expected_result):
    assert mastprov.MastImageProv.validate(params) == expected_result


@pytest.mark.parametrize(
    "params, expected_result",
    [
        (file_params, {"data": "alp", "shot": 30420, "run": None, "graph": False}),
        (
            {"data": "ip", "shot": 30420},
            {"data": "ip", "shot": 30420, "run": None, "graph": False},
        ),
    ],
)
def test_parse_params(params, expected_result):
    assert mastprov.parse_params(params) == expected_result


def test_parse_params_arbitrary_args():
    assert mastprov.parse_params("ip", 30420, 0, True) == {
        "data": "ip",
        "shot": 30420,
        "run": 0,
        "graph": True,
    }
